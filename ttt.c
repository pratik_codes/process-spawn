#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

#define	NUM_CHILD	5  
#define WITH_SIG

int   CREATED_CHILDS = 0;
pid_t CHILD_IDS[NUM_CHILD]; 

/*
Keyboard interrupt handler.

*/
#ifdef WITH_SIG
char interrupt_flag = 0;

void killedChild() { 
	printf("child[%d]: Termination of the process.\n", getpid());
	exit(1);
}

void keyboardInterrupt() {
	printf("parent[%i]: A key press has been received.\n", getpid());
	interrupt_flag = 1;
}
#endif




void spawnChild(int i){
	
	pid_t child_pid = fork(); // Forking new Process
	if(child_pid == -1){
		fprintf(stderr, "An error in creating new fork.\n");
		//Killing all children.		
		int j;
		for(j = 0; j < CREATED_CHILDS; ++j){
			kill(CHILD_IDS[j], SIGTERM);
		}
		exit (1);
	}
	
	//Copying all the children to a buffer on successfull creation.
	if(child_pid > 0){
		CHILD_IDS[i] = child_pid;
		CREATED_CHILDS++;
	}
	
	if(child_pid == 0){
		
		#ifdef WITH_SIG
			signal (SIGINT,killedChild); // Calling the interrupt for killing the process.
		#endif

		printf("*child[%d]: has been created\n", (int) getpid());
		printf("->child[%d]\n", (int) getpid());
		sleep(10);
		printf("child[%d]: 10s Task has been completed.\n", (int) getpid());
		exit(0);
	}

}

int main(){
	//int tsig_id = (int) getpid();
	int i, j;
	printf("parent[%d]: has been spawned.\n", (int) getpid());
	for(i = 0; i < NUM_CHILD; ++i){
			
	  #ifdef WITH_SIG
			for(j = 0; j < NSIG; ++j){ 
				sigignore(j);
			}
			signal (SIGCHLD, SIG_DFL);
			signal (SIGINT, keyboardInterrupt); 
		#endif

		spawnChild(i);
		sleep(1);

		#ifdef WITH_SIG
			if (interrupt_flag == 1){
				printf("parent[%i]: Interrupt of the creation process!\n", getpid());
				kill(-2, SIGTERM);
				break;
			}
		#endif
	}

	//#ifdef WITH_SIG
	if(CREATED_CHILDS == NUM_CHILD) 
		printf("parent[%d]: All processes have been created.\n", (int) getpid());

	int child_status;
	pid_t w[CREATED_CHILDS];
	int exit_code[CREATED_CHILDS];
	
	for(i = 0; i < CREATED_CHILDS; i++){
		w[i] = wait(&child_status);
		if(w[i] == -1)
			break;
		else{	
			if(WIFEXITED (child_status))	
				exit_code[i] = WEXITSTATUS(child_status);
		}
	}

	
	printf("parent[%d]: Terminated %d processes.\n", (int) getpid(), CREATED_CHILDS);
	


	#ifdef WITH_SIG
		for(j=0; j<NSIG; j++)
			signal(j, SIG_DFL);
	#endif

	return 0;
}
