    #include <stdio.h>
    #include <stdlib.h>
    #include <sys/wait.h>
    #include <unistd.h>
    #include <errno.h>
    #include <signal.h>
    #include <string.h>
    #include <time.h>
     
    #define NUM_CHILD 5
    #define WITH_SIGNALS
     
    #define ERR(source) (fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
                         perror(source),kill(0,SIGKILL),\
    		     		         exit(EXIT_FAILURE))
     
    #ifdef WITH_SIGNALS
     
    volatile sig_atomic_t last_signal = 0;
     
    void sethandler( void (*f)(int), int sigNo )
    {
      struct sigaction act;
      memset( &act, 0, sizeof(struct sigaction) );
      act.sa_handler = f;
      if ( sigaction(sigNo, &act, NULL) == -1 ) ERR("sigaction: ");
    }
     
    void sig_handler( int sig )
    {
      printf( "parent[%d] received signal %d!\n", getpid(), sig );
      last_signal = sig;
    }
     
    void child_sig_handler( int sig )
    {
      printf( "child[%d] received signal %d!\n", getpid(), sig );
      printf( "child[%d] Terminating!\n", getpid() );
      exit( EXIT_SUCCESS );
    }
     
    #endif /* WITH_SIGNALS */
     
    void child_work( void )
    {
      printf( "child[%d] I was spawned by parent PID %d!\n", getpid(), getppid() );
      sleep( 10 );
      printf( "child[%d] Terminating!\n", getpid() );
    }
     
    int create_children( int n )
    {
      for ( int i = 0; i < n; ++i )
      {
      #ifdef WITH_SIGNALS
        if ( last_signal == SIGINT )
          return i; /* main will handle the rest */
      #endif /* WITH_SIGNALS */
     
        switch ( fork() )
        {
          case 0:
          #ifdef WITH_SIGNALS
            sethandler( SIG_IGN, SIGINT );
            sethandler( child_sig_handler, SIGTERM );
          #endif /* WITH_SIGNALS */
            child_work();
            exit( EXIT_SUCCESS );
          case -1:
            printf( "parent[%d] Error occured in fork().\n", getpid() );
            printf( "parent[%d] Killing all children.\n", getpid() );
          #ifdef WITH_SIGNALS
            kill( 0, SIGTERM );
          #endif /* WITH_SIGNALS */
            return -1;
        }
     
        sleep( 1 );
      }
     
      printf( "parent[%d] %d children spawned!\n", getpid(), n );
      return n;
    }
     
    int main( int argc, char** argv )
    {
      printf( "parent[%d] Alive!\n", getpid() );
     
    #ifdef WITH_SIGNALS
      sigset_t mask, oldmask;
      sigemptyset( &mask );
      sigfillset( &mask );
      sigdelset( &mask, SIGCHLD );
      sigdelset( &mask, SIGINT );
      sigdelset( &mask, SIGTERM );
      sigprocmask( SIG_BLOCK, &mask, &oldmask );
     
      sethandler( SIG_IGN, SIGTERM );
      sethandler( sig_handler, SIGINT );
    #endif /* WITH_SIGNALS */
     
      int spawned_children = create_children( NUM_CHILD );
     
    #ifdef WITH_SIGNALS
      if ( spawned_children == -1 )
      {
        while( wait(NULL) > 0 );
     
        sigprocmask( SIG_UNBLOCK, &mask, NULL );
        sethandler( SIG_DFL, SIGTERM );
        sethandler( SIG_DFL, SIGINT );
     
        printf( "parent[%d] Terminating with exit code 1.\n", getpid() );
        return 1;
      }
    #endif /* WITH_SIGNALS */
     
      int dead_children = 0;
     
      while ( 1 )
      {
      #ifdef WITH_SIGNALS
        if ( last_signal == SIGINT )
        {
          printf( "parent[%d] Killing all children.\n", getpid() );
          kill( 0, SIGTERM );
          last_signal = 0;
        }
      #endif /* WITH_SIGNALS */

     	//Making sure all the children died.
        pid_t pid;
        pid = waitpid( 0, NULL, WNOHANG );
        if ( pid > 0 ) dead_children++;
        if ( pid < 0 && errno != ECHILD )
          ERR( "waitpid: " );
        if ( dead_children == spawned_children )
        {
          printf( "parent[%d] %d children died.\n", getpid(), dead_children );
          break;
        }
      }
     
    #ifdef WITH_SIGNALS
      sigprocmask( SIG_UNBLOCK, &mask, NULL );
      sethandler( SIG_DFL, SIGTERM );
      sethandler( SIG_DFL, SIGINT );
    #endif /* WITH_SIGNALS */
     
      printf( "parent[%d] Terminating with exit code 0.\n", getpid() );
      return EXIT_SUCCESS;
    }
